20.3.0  : - upgraded DICOMweb to 1.1
          - Added SCHED_MAX_QUEUED_JOBS and SCHED_MAX_CONCURRENT_JOBS.
          - Deprecated SCHED_MAX_JOBS.
          - add SCHED_JOBS_HISTORY_SIZE
          - BREAKING CHANGE: sub-option "HasWadoRsUniversalTransferSyntax" must be set to "false" 
            in the DW_SERVERS configuration option of the DICOMweb module, if contacting an
            Orthanc server that is equipped with release <= 1.0 of the DICOMweb plugin.
            https://book.orthanc-server.com/plugins/dicomweb.html#client-related-options
          - add LUA_OPTIONS env var
          - add TRANSFERS_BUCKET_SIZE env var
          - add TRANSFERS_CACHE_SIZE env var
          - add TRANSFERS_MAX_PUSH_TRANSACTIONS env var
          - add STORAGE_ACCESS_ON_FIND en var
          - add DICOM_QUERY_RETRIEVE_SIZE env var
          - add DICOM_DICTIONARY env var
20.2.0  : - upgraded MSSQL to 1.1.0, added MSSQL_MAXIMUM_CONNECTION_RETRIES & MSSQL_CONNECTION_RETRY_INTERVAL
20.1.0  : - Introduced gcp-dicom setup procedure
          - upgraded WVB to 1.3.1
          - upgraded WVB_ALPHA to 1.3.1
          - upgraded WVP to 1.3.1.0
          - upgraded WVP_ALPHA to 1.3.1.0
19.11.2 : - added INSTANCE_INFO_CACHE_ENABLED env var for viewer PRO
19.11.1 : - added INSTANCE_INFO_CACHE_ENABLED env var
19.10.4 : - really added HTTP_REQUEST_TIMEOUT (default = 30)
19.10.3 : - added HTTP_REQUEST_TIMEOUT (default = 30)
19.10.2 : - upgraded Orthanc to 1.5.8
          - BREAKING CHANGE: added EXECUTE_LUA_ENABLED settings (default = false).
            This options, when set to false, disables the /tools/execute-script Rest API route.
            If you decide to set EXECUTE_LUA_ENABLED to true, make sure that you know what you're
            doing and that this API route is protected from the outside world or secured by another mean.
            Anyone could execute any code on your machine from this route !
          - BREAKING CHANGE: added AC_AUTHENTICATION_ENABLED setting (default = true).  
            It means that you'll need a login/pwd to access orthanc !  These login/pwd shall be defined 
            in AC_REGISTERED_USERS.  If no login/pwd are defined, Orthanc will generate a default user (orthanc:orthanc)
            and will display a warning in the Orthanc Explorer stating that your setup is insecure.
            If you decide to set AC_AUTHENTICATION_ENABLED to false, make sure that you know what you're
            doing and that your Orthanc is either not accessible from the outside world or secured by another mean.
            Note that if AC_AUTHENTICATION_ENABLED is false, Orthanc Explorer will display a warning stating that your
            setup is insecure.
19.10.1 : - upgraded WVB_ALPHA to 7aff3d60 (PatientBirthDate in overlay + small fixes)
          - upgraded WVP_ALPHA to aa6dbe8f (PatientBirthDate in overlay + small fixes)
19.9.4  : - upgraded WVB to 1.3.0
          - upgraded WVB_ALPHA to 1.3.0
          - upgraded WVP to 1.3.0.0
          - upgraded WVP_ALPHA to 1.3.0.0
19.9.3 :  - added WVB_REFERENCE_LINES_ENABLED, WVB_CROSS_HAIR_ENABLED, WVB_SYNCHRONIZED_BROWSING_ENABLED settings
          - added WVP_REFERENCE_LINES_ENABLED, WVP_CROSS_HAIR_ENABLED, WVP_SYNCHRONIZED_BROWSING_ENABLED settings
19.9.2 :  - upgraded WVP_ALPHA to adf96ee7 (3 new settings in config file - this is also a 1.3.0.0 release candidate)
          - upgraded WVB_ALPHA to b80cd32a (3 new settings in config file - this is also a 1.3.0 release candidate)
19.9.1 :  - added AC_AUTHENTICATION_ENABLED setting (default = false).  Warning, in future releases (featuring Orthanc 1.5.8,
            this setting will be set to true by default)
          - base ubuntu:16.04 image has been updated.  The tzdata package is now installed explicitely.
          - upgraded WVP_ALPHA to 136f6137 (new pickableStudyIds & selectedStudyIds query args)
          - upgraded WVB_ALPHA to 2b16a5e6 (new pickableStudyIds & selectedStudyIds query args)
19.7.1 :  - upgraded WVP_ALPHA to f63878c (uuid in annotations)
          - upgraded WVB_ALPHA to 871b22f4 (uuid in annotations)
19.6.4 :  - upgraded WVP_ALPHA to e3b6a91 (fix query args)
          - upgraded WVB_ALPHA to 4419b1b8 (fix query args)
19.6.3 :  - upgraded WVP_ALPHA to acf5875 (query args passed as HTTP headers to backend + language query arg)
          - upgraded WVB_ALPHA to 4431bea9 (query args passed as HTTP headers to backend + language query arg)
          - added WVB_PRINT_ENABLED & WVP_PRINT_ENABLED settings
          - added WVB_DOWNLOAD_AS_JPEG_ENABLED & WVP_DOWNLOAD_AS_JPEG_ENABLED settings
19.6.2 :  - upgraded Orthanc to 1.5.7
          - upgraded DICOMweb to 1.0
          - added Google Cloud Platform plugin 1.0
19.6.1 :  - upgraded WVB_ALPHA to 45f6d268 (fix Download as jpeg + fix synchronized browsing)
          - upgraded WVP_ALPHA to 03093e6 (fix Download as jpeg + fix synchronized browsing)
19.6.0 :  - upgraded WVB_ALPHA to c9110211 (Download as jpeg)
          - upgraded WVP_ALPHA to 84de39e (Download as jpeg)
19.4.1 :  - upgraded WVP_ALPHA to d4b6c4b
19.3.4 :  - upgraded WVP_ALPHA to cd1ad9a
19.3.3 :  - Add USERMETADATA setting
19.3.2 :  - added transfers accelerator plugin 1.0
19.3.1 :  - upgraded Orthanc to 1.5.6
          - upgraded PostgreSQL plugins to 3.2
          - upgraded Orthanc Web viewer to 2.5
          - upgraded DICOMweb plugin to 0.6
          - upgraded WSI plugin to 0.6
19.2.2 :  - Upgraded Orthanc to 1.5.5
19.2.1 :  - Upgraded Orthanc to 1.5.4
          - Upgraded PostgreSQL to 3.1
          - Add SERIES_TO_IGNORE setting (WVB and WBP)
          - Add NO_JOBS setting to add the --no-jobs option at Orthanc command line
          - Add UNLOCK setting to add the --unlock option at Orthanc command line
19.1.1  : - upgraded Orthanc to 1.5.3
          - upgraded PostgreSQL to 3.0
          - upgraded MySQL to 2.0
          - Add HTTP_KEEP_ALIVE setting
          - Add TCP_NODELAY setting
          - Add SCHED_SAVE_JOBS and SCHED_MAX_JOBS settings
18.12.3 : - upgraded Orthanc to 1.5.1
18.12.2 : - upgraded WVB_ALPHA to 7c4d1a44 (Cross-Hair)
          - upgraded WVP_ALPHA to 6b11ef5 (Cross-Hair)
18.12.1 : - upgraded blob storage plugin to 0.3.2 (previous version was not compatible with ubuntu 16.04)
18.12.0 : - upgraded Orthanc to 1.5.0
          - upgraded WVP_ALPHA to 68ad0db
18.11.1 : - upgraded WVB to 1.2.0
          - upgraded WVB_ALPHA to 1.2.0
          - upgraded WVP to 1.2.0.0
          - upgraded WVP_ALPHA to 1.2.0.0
18.11.0 : - DICOM_SYNCHRONOUS_CMOVE now set to "true" by default
18.9.5  : - Fix double prefix issue for DICOM_ALWAYS_ALLOW_ECHO_ENABLED and DICOM_ALWAYS_ALLOW_STORE_ENABLED
          - Add DICOM_SYNCHRONOUS_CMOVE setting
18.9.4  : - Add IMPORT_OVERWRITE_INSTANCES setting
18.9.3  : - upgraded Orthanc to 1.4.2
18.9.2  : - upgraded WVB_ALPHA to 090cd828 (improved synchronization)
18.9.1  : - upgraded WVP_ALPHA to 7090062 (fix copy-paste in Live-Share)
          - Bug fix: in 18.8.1: WVP and WVP_ALPHA were inverted
18.8.1  : - upgraded Authorization plugin to 0.2.2
          - Add WVB_OPEN_ALL_PATIENT_STUDIES and WVP_OPEN_ALL_PATIENT_STUDIES settings
          - upgraded WVP_ALPHA to 9c6b4d3 (fix for print + JP2K display)
          - internal: the build has been optimized and is now just a matter of assembling
            binaries that have been produced by other build-systems.  Note that we use LSB binaries
            for Orthanc and some of its plugins: OrthancViewer, ModalityWorklist, DicomWeb,
            PostgreSQL, MySQL, WSI.
18.7.3  : - added env var DICOM_UNKNOWN_SOP_CLASS_ACCEPTED
18.7.2  : - Introduced WVB_ALPHA_ENABLED to use alpha version of the Osimis WebViewer plugin
          - upgraded MySQL to 1.1
          - upgraded WVP_ALPHA to 73e6b64
          - upgraded WVB_ALPHA to c3ac8fac
18.7.1  : - upgraded Orthanc to 1.4.1
          - upgraded PostgreSQL to 2.2
          - added MySQL 1.0
18.7.0  : - upgraded Orthanc to 1.4.0
18.6.0  : - Deprecated LISTENER_LISTEN_ALL_ADDR
          - Introduced access-control setup procedure
18.5.2  : - upgraded WVP_ALPHA to 1.1.1.0
          - upgraded WVP to 1.1.1.0
          - upgraded WVB to 1.1.1
18.5.1  : - added env vars WV{B,P}_COMBINED_TOOL_ENABLED, DEFAULT_SELECTED_TOOL, LANGUAGE, TOGGLE_OVERLAY_TEXT_BUTTON_ENABLED
18.5.0  : - upgraded WVP_ALPHA to 1.1.0.0
          - upgraded WVP to 1.1.0.0
          - upgraded WVB to 1.1.0
18.4.4  : - upgraded DicomWeb to 0.5
          - upgraded PostgreSQL to 2.1
          - upgraded Orthanc Web viewer to 2.4
          - upgraded WSI to 0.5
18.4.3  : - upgraded Orthanc to 1.3.2
          - upgraded MSSQL to 1.0.0
18.4.2  : - added TRACE_ENABLED env var
18.4.1  : - upgraded WVP_ALPHA to 2cfa333 (fix shortcuts in Liveshare + fix default windowing in some RGB images) 
18.4.0  : - added env vars for authorization plugin
18.3.5  : - added WV{B,P}_KEYBOARD_SHORTCUTS_ENABLED
18.3.4  : - upgraded WVP_ALPHA to 55390bf (fix Liveshare + some opti)
18.3.3  : - forget it !
18.3.2  : - downgraded MSSQL back to 0.5.0 (0.6.1 is not compatible with Orthanc 1.3.1)
18.3.1  : - upgraded Authorization plugin to 0.2.1 + MSSQL to 0.6.1 + WVP_ALPHA to ae8b463
18.3.0  : - upgraded Authorization plugin to 0.2.0 + WVP_ALPHA to 2c43e74 (shortcuts + combined tool + synchronized browsing)
18.1.6  : - upgraded WVB to 1.0.2 and WVP to 1.0.2.0 (Annotation storage hot-fix)
18.1.5  : - added DW_STOW_MAX_INSTANCES & DW_STOW_MAX_SIZE
          - upgraded WVP_ALPHA to df1592b (previous build was not available) 
18.1.4  : - upgraded WVP_ALPHA to 335ab7a (for correct support of KeyImageCaptureEnabled)
18.1.3  : - added WVB_KEY_IMAGE_CAPTURE_ENABLED
18.1.2  : - added WVP_KEY_IMAGE_CAPTURE_ENABLED
18.1.1  : - upgraded WVP_ALPHA to 13ce4ba (to fix the windowingPresets in Lify)
17.12.2 : - include ca-certificates in image and use them by default for Orthanc HttpClient
